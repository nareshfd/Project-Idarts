const InitialState = {
	userData: {}
};

const Reducer = (state = InitialState, action) => {
	switch (action.type) {
		case 'GET_NEWS':
			return { ...state };
		case 'NEWS_RECEIVED':
			return { ...state, data: action.dataJson };
		case 'AUTH_START':
			return { ...state, data: action.dataJson };

		default:
			return state;
	}
};

export default Reducer;
