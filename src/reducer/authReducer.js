import * as actionTypes from '../action/actionTypes';

const initialstate = {
	token: null,
	userid: null,
	error: null,
	loading: false
};

export const reducer = (state = { initialstate }, action) => {
	switch (action.type) {
		case actionTypes.AUTH_START:
			return {
				...state,
				loginUser: action.payload
			};
		case actionTypes.AUTH_SUCCESS:
			return {
				...state,
				loginUser: action.payload
			};
		case actionTypes.AUTH_FAIL:
			return {
				...state,
				loginUser: action.payload
			};
		default:
			return state;
	}
};
