import * as actionTypes from './actionTypes';
import Axios from 'axios';

export const getNews = () => ({
	type: 'GET_NEWS',
});

export const authStart = () => {
	return {
		type: actionTypes.AUTH_START,
	};
};

export const authsuccess = (authData) => {
	return {
		type: actionTypes.AUTH_SUCCESS,
		authData: authData,
	};
};

export const authFail = (error) => {
	return {
		type: actionTypes.AUTH_FAIL,
		error: error,
	};
};

export const auth = (email, password) => {
	// return {
	// 	type: actionTypes.AUTH_START,
	// 	payload: { email, password }
	// };
	return (dispatch) => {
		dispatch(authStart());
	};
};

export const loginUser = (email, password) => {
	// dispatch(authStart());
	debugger;
	const authData = {
		email: email,
		password: password,
		returnSecureToken: true,
	};
	Axios.post('https://fir-4369e.firebaseio.com/idarts', authData)
		.then((res) => ({
			// console.log(res),
			type: actionTypes.AUTH_START,
		}))
		.catch((res) => ({
			type: actionTypes.AUTH_START,
		}));
};
