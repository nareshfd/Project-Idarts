import { put, takeLatest, all } from 'redux-saga/effects';
import { loginUser, authsuccess, authStart, authFail } from '../action/auth';
import { AUTH_START } from '../action/actionTypes';
import Axios from 'axios';

function* fetchNews() {
  const dataJson = yield fetch('https://fir-4369e.firebaseio.com/idarts.json').then((response) =>
    response.json(),
  );

  yield put({ type: 'NEWS_RECEIVED', dataJson: dataJson });
}

function* UserLogin(payload) {
  dispatchEvent(authStart());
  const authData = {
    email: payload.email,
    password: payload.password,
    returnSecureToken: true,
  };
  const login = Axios.post(
    'https://identitytoolkit.googleapis.com/v1/accounts:signInWithCustomToken?key=AIzaSyDSD9z5Eo5YztBB12349yqf5972VNMyDHCyPUiWws',
    authData,
  )
    .then((res) => console.log(res))
    .catch((err) => {
      console.log(err);
      dispatchEvent(authFail);
    });

  yield put({ type: 'AUTH_START', login: login });
}

// const act = ('GET_NEWS', fetchNews, 'AUTH_START', UserLogin);

function* UserLogin(payload) {
	dispatchEvent(authStart());
	const authData = {
		email: payload.email,
		password: payload.password,
		returnSecureToken: true
	};
	const login = Axios.post(
		'https://identitytoolkit.googleapis.com/v1/accounts:signInWithCustomToken?key=AIzaSyDSD9z5Eo5YztBB12349yqf5972VNMyDHCyPUiWws',
		authData
	)
		.then(res => console.log(res))
		.catch(err => {
			console.log(err);
			dispatchEvent(authFail);
		});

	yield put({ type: 'AUTH_START', login: login });
}

function* actionWatcher() {
  yield takeLatest('GET_NEWS', fetchNews, 'AUTH_START', UserLogin);
}
export default function* rootSaga() {
  yield all([actionWatcher()]);
}
