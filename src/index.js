import React from 'react';
import createSagaMiddleware from 'redux-saga';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { logger } from 'redux-logger';
import 'bootstrap/dist/css/bootstrap.min.css';
import reducer from './reducer/reducer';
import App from './App';
import rootSaga from './sagas/sagas';
import { composeWithDevTools } from 'redux-devtools-extension';
import { default as ReduxThunk } from 'redux-thunk';

const sagaMiddleware = createSagaMiddleware();
// const ThunkMiddleware = [ReduxThunk];

// const store = createStore(reducer, applyMiddleware(sagaMiddleware, logger));

const composeEnhancers = composeWithDevTools({
	// options like actionSanitizer, stateSanitizer
});
const store = createStore(
	reducer,
	composeEnhancers(applyMiddleware(sagaMiddleware, logger))
);

sagaMiddleware.run(rootSaga);
render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
if (module.hot) {
	module.hot.accept(App);
}
