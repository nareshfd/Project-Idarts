import React from 'react';
import './App.css';
import Dashboard from './Component/Dashboard';
import Inav from './Component/Navbar/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './Component/Form/Login';
import NotFoundPage from './Component/NotFoundPage';

function App() {
	return (
		<div className='App'>
			<div className='container-fluid px-0'>
				<Inav />
				<div className='container-fluid mt-5'>
					<Router>
						<Switch>
							<Route path='/login' exact component={Login} />
							<Route path='/' exact component={Dashboard} />
							<Route path='/*' component={NotFoundPage} />

							<Dashboard />
						</Switch>
					</Router>
				</div>
			</div>
		</div>
	);
}

export default App;
