import React, { Component } from 'react';
import { Form, FormGroup, Input, Button } from 'reactstrap';
import logo from '../../assets/logo.svg';
import { loginUser } from '../../action/actions';
import './login.css';
import * as actions from '../../action/actions';
import { connect } from 'react-redux';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  onChange = (e) => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    let data = [];
    data = this.state;

    const { email, password } = data;
    console.log(email, password);
    this.props.login(email, password);
  };

  render() {
    const { email, password } = this.state;
    return (
      <div className="row">
        <div className="col login_form">
          <Form onSubmit={this.handleSubmit}>
            <FormGroup className="text-left">
              <div className="container-fluid">
                <h2 className=" text-center pb-1">
                  <img
                    src={logo}
                    className="img-responsive pb-0"
                    style={{
                      width: '100%',
                      height: '60px',
                      paddingBottom: '0',
                    }}
                    alt="logo"
                  />
                </h2>
              </div>
              <Input
                type="email"
                name="email"
                value={email}
                id="email"
                placeholder="Enter Your Email"
                onChange={this.onChange}
              />
            </FormGroup>
            <FormGroup className="text-left">
              <Input
                type="password"
                name="password"
                id="password"
                value={password}
                placeholder="Enter Your password"
                onChange={this.onChange}
              />
            </FormGroup>
            <FormGroup className="text-left">
              <Button color="primary" size="sm" block>
                Login
              </Button>
            </FormGroup>
          </Form>
        </div>
      </div>
    );
  }
}

// const mapDispatchToProps = (dispatch) => {
// 	return {
// 		loginUser,
// 		// loginUser: (email, password) => dispatch(actions.auth(email, password)),
// 	};
// };

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, password) => {
      dispatch(loginUser(email, password));
    },
  };
};

const mapStateToProps = (dispatch) => {
  return {
    loginUser: (email, password) => dispatch(actions.auth(email, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
