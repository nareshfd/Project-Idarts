import React from 'react';

export const Sharetab = data => {
	let bar = parseInt(data.Return);
	let barPv = parseInt(data.PortfolioValue);
	let MV = (data.Price * data.qty).toFixed(2);
	// console.log(MV);

	let bar_pos = bar < 0 ? bar * -1 : bar;

	return (
		<div className='row px-2'>
			<div className='col-md-2 price'>
				<span className='d-block text-dark text-center'>{data.Script}</span>
				<span className='d-inline'>
					<span className='dollar'>$</span> {data.Price}
				</span>
			</div>
			<div className='col-md-2  text-left'>
				<img
					src='https://upload.wikimedia.org/wikipedia/commons/4/4e/IShares_by_BlackRock_Logo.png'
					alt='ishares'
					className='img-fluid'
				/>
				<p className='share_det'>
					S & P 500 index
					<span className='sm-text d-block'>{data.data}</span>
				</p>
			</div>
			<div className='col-md-2 det text-left'>
				<p>
					<i className='fas px-1 fa-database'></i> Quantity :
					<span className='float-right dtab_cell '>{data.qty} </span>
				</p>
				<p>
					<i className='fas px-1 fa-at'></i> Avg.Cost :
					<span className='float-right dtab_cell'>$ {data.AvgCost} </span>
				</p>
				<p>
					<i className='fas px-1 fa-money-bill-alt'></i> Invested Amt :
					<span className='float-right dtab_cell '>$ {data.IA} </span>
				</p>
			</div>
			<div className='col-md-2 text-center px-3'>
				<div className='col p-0'>
					<span className='text-dark'>Market Value : $ {MV} </span>
					<span>%&nbsp;of Portfolio Value = {barPv}</span>
				</div>

				<div className='progress'>
					<div
						className='progress-bar'
						role='progressbar'
						style={{ width: barPv }}
						aria-valuenow='25'
						aria-valuemin='0'
						aria-valuemax='100'></div>
				</div>
			</div>
			<div className='col-md-2 text-center px-3'>
				<p className='text-dark'>Unrealized PL : $ {data.PL}</p>
				<p className='col-md-12 d-block text-left p-0'>
					<span className='text-left d-inline block'>% Return =</span>
					<span className={'text-right ' + (bar < 0 ? 'neg' : 'pos')}>
						<i className='fas fa-caret-down'></i> {data.Return}
					</span>
				</p>

				<div className='progress'>
					<div
						className={
							'progress-bar mx-auto' +
							' ' +
							(bar < 0 ? 'bg-danger' : 'bg-success')
						}
						role='progressbar'
						style={{ width: bar_pos }}
						aria-valuenow='25'
						aria-valuemin='0'
						aria-valuemax='100'></div>
				</div>
			</div>

			<div className='col-md-2 px-1'>
				<button type='button' className='btn btn-outline-danger'>
					Buy
				</button>

				<button type='button' className='btn  btn-outline-danger  '>
					Sell
				</button>
			</div>
		</div>
	);
};
