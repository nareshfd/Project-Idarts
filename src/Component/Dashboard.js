import React, { Component } from 'react';
import { Sharetab } from './Sharetab';
import { connect } from 'react-redux';
import { getNews } from '../action/actions';
import { v4 as uuidv4 } from 'uuid';
import Chart from './chart';
import './Dashboard.css';

class Dashboard extends Component {
  state = {
    data: [],
  };
  componentDidMount() {
    this.props.getNews();
  }

  render() {
    const { getdata } = this.props;
    let { data = [] } = getdata;

    const mut_f = data.filter(function (person) {
      return person.data === 'MF';
    });

    const ETF = data.filter(function (person) {
      return person.data === 'ETF';
    });

    var MfScore = mut_f.reduce(function (sum, person) {
      let MVMF = person.Price * person.Quantity;
      return sum + parseFloat(MVMF);
    }, 0);

    var ETFScore = ETF.reduce(function (sum, person) {
      let MVETF = (person.Price * person.Quantity).toFixed(2);

      return sum + parseFloat(MVETF);
    }, 0);

    return (
      <div className="container-fluid dashboard">
        <div className="row">
          {data.map((data) => {
            return (
              <div className="col-9 share_tab" id={uuidv4()} key={uuidv4()}>
                <Sharetab
                  AvgCost={data.AvgCost}
                  qty={data.Quantity}
                  Price={data.Price}
                  PL={data.Unrealized_PL}
                  Return={data.Return}
                  IA={data.InvestedAmount}
                  PortfolioValue={data.PortfolioValue}
                  data={data.data}
                  Script={data.Script}
                />
              </div>
            );
          })}
          <div className="col-2 chart">
            <div className="p-2 mt-2 pb-0 text-left text-muted">
              Portfolio
              <span className="float-right">
                <div className="btn-group">
                  <button
                    type="button"
                    className="btn btn-outline-info btn-sm dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Asset Wise
                  </button>
                  <div className="dropdown-menu dropdown-menu-right">
                    <button className="dropdown-item" type="button">
                      Action
                    </button>
                    <button className="dropdown-item" type="button">
                      Another action
                    </button>
                    <button className="dropdown-item" type="button">
                      Something else here
                    </button>
                  </div>
                </div>
              </span>
            </div>

            <Chart id={uuidv4()} key={uuidv4()} ETFScore={ETFScore} MfScore={MfScore} />
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  getNews: getNews,
};

const mapStateToProps = (state) => ({
  getdata: state,
});

// Dashboard = connect()(Dashboard);
Dashboard = connect(mapStateToProps, mapDispatchToProps)(Dashboard);

export default Dashboard;

// export default Dashboard;
