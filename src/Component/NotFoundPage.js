import React from 'react';
import { Link } from 'react-router-dom';
// import PageNotFound from '../assets/404.png';
class NotFoundPage extends React.Component {
	render() {
		return (
			<div className='text-center pagenotfound'>
				<h1 className='text-dark pb-5'> 404 page not found</h1>
				<Link className='btn btn-danger' to='/'>
					Go to Home{' '}
				</Link>
			</div>
		);
	}
}
export default NotFoundPage;
