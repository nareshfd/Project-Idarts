// import React from 'react';
import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
} from 'reactstrap';
// import { DiGhostSmall } from 'react-icons/di';
import logo from '../../assets/logo.svg';
// import SignedInLinks from './SignedInLinks';
// import SignedOutLinks from './SignedOutLinks';

const Inav = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  debugger;
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="white" className="navbar-light navbar-expand-sm  box-shadow" fixed="top">
        <NavbarBrand className="pl-5 navbar-brand1 " href="/">
          <img src={logo} className="img-responsive logo" alt="logo" />
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto pr-5" navbar>
            <SignedInLinks />
            <SignedOutLinks />
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
};

export const SignedOutLinks = () => {
  return (
    <>
      <NavItem>
        <NavLink href="/login">
          <Button outline color="danger" size="sm">
            Login
          </Button>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink href="/signUp">
          <Button outline color="warning" size="sm">
            SignUp
          </Button>
        </NavLink>
      </NavItem>
    </>
  );
};

export const SignedInLinks = () => {
  return (
    <>
      <NavItem>
        <NavLink href="/">DashBoard</NavLink>
      </NavItem>

      <NavItem>
        <NavLink href="/">Only MF</NavLink>
      </NavItem>
      <NavItem>
        <NavLink href="/">Only ETF</NavLink>
      </NavItem>

      <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
          UserName
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem>Option 1</DropdownItem>
          <DropdownItem>Option 2</DropdownItem>
          <DropdownItem divider />
          <DropdownItem>Sign Out</DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    </>
  );
};

export default Inav;
