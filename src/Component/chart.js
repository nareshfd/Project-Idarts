import React, { PureComponent } from 'react';
import { PieChart, Pie, Cell, Legend } from 'recharts';

const COLORS = ['#FFBB28', '#FF8042'];

export default class Example extends PureComponent {
	render() {
		// let MFScore = this.props.MfScore;
		// let ETFScore = this.props.ETFScore;

		let MFScore = this.props.ETFScore;
		let ETFScore = this.props.MfScore;

		const data = [
			{ name: 'ETF', value: MFScore },
			{ name: 'MF', value: ETFScore }
		];

		return (
			<PieChart width={150} height={150} onMouseEnter={this.onPieEnter}>
				<Pie
					data={data}
					innerRadius={40}
					outerRadius={60}
					fill='#8884d8'
					paddingAngle={0}
					dataKey='value'>
					{data.map((entry, index) => (
						<Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
					))}
				</Pie>
				<Legend
					iconSize={10}
					width={120}
					height={140}
					// layout='horizontal'
					verticalAlign='middle'
				/>
			</PieChart>
		);
	}
}
